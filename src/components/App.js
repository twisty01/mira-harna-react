import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import LandingPage from './LandingPage';
import SignUpPage from './auth/SignUp';
import SignInPage from './auth/SignIn';
import PasswordForgetPage from './admin/PasswordForget';
import AccountPage from './admin/Account';
import * as routes from '../constants/routes';
import { firebase } from '../firebase';
import withAuthentication from './auth/withAuthentication';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      authUser: null,
    };
  }

  componentDidMount() {
    firebase.auth.onAuthStateChanged(
      authUser => { authUser ? this.setState({ authUser }) : this.setState({ authUser: null });
    });
  }

  render() {
    return (
      <Router>
        <React.Fragment>
          <Route
            path={routes.ADMIN}
            component={AccountPage}
          />
          <Route
            exact path={routes.SIGN_UP}
            component={SignUpPage}
          />
          <Route
            exact path={routes.SIGN_IN}
            component={SignInPage}
          />
          <Route
            exact path={routes.PASSWORD_FORGET}
            component={PasswordForgetPage}
          />
          <Route 
            path={routes.HOME}
            component={LandingPage}
          />
        </React.Fragment>
      </Router>
    );
  }
};


export default withAuthentication(App);