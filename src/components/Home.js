import React from 'react';
import {Link} from 'react-router-dom';

export default function HomePage({home, categories, pages, setLocale, locale, localeOptions}){
  return (
  <div style={ { backgroundImage:  `url(${home && home.image})`} } className="home-page flex flex-vspace flex-wrap">

      <header className="w100 flex flex-vcenter">
        <nav className="pages w100">
          <ul className="flex flex-hright">
              {localeOptions.map(o => o !== locale && <li key={o}><span className="nav-link" style={home.color ? {color: home.color} : {}} onClick={()=>setLocale(o)}>{o}</span></li>)}
              {pages &&  Object.values(pages).map(p => <li key={p.slug}><Link className="nav-link" style={home.color ? {color: home.color} : {}} to={`/page/${p.slug}`}>{p.name}</Link></li>)}
          </ul>  
        </nav>
      </header>

      <section className="w100 home-header">
        <h1 className="w100" style={home.background ? {color: home.background} : {}} >Jaromír Harna</h1>
        <nav className="w100 categories">
          <ul className="flex">
            {categories && Object.values(categories).map(c => <li key={c.slug}><Link className="nav-link" style={home.background ? {color: home.background} : {}} to={`/category/${c.slug}`}>{c.name}</Link></li>)}
          </ul>
        </nav>
      </section>
  </div>
)};