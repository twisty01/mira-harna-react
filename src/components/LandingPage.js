import React from 'react';
import { Route } from 'react-router-dom'
import HomePage from './Home';
import * as db from '../firebase/db';
import PagePage from './Page';
import CategoryPage from './Category';
import Header from './Header';
import * as routes from '../constants/routes';
import { defaultLocale, localeOptions } from '../constants/locale';
import slug from '../helpers/slugify';
import { Home } from '../helpers/scopedStyles';

export default class LadingPage extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            loading : false,
            locale: defaultLocale,
            categories: [],
            pages: [],
            home: {}
        }
        // raw data
        this.categories = [];
        this.pages = [];
    }

    async init(){
        let locale = localStorage.getItem("locale");
        if ( localeOptions.indexOf( locale ) === -1) { locale = defaultLocale; }
        this.categories = await db.getCategoriesAsArray();
        this.photos = await db.getPhotos();
        this.pages = await db.getPagesAsArray();
        const home = await db.getHome(); 
        const { categories, pages }  = this.updateDataByLocale(locale);
        this.setState({
            home,
            loading: false,
            locale,
            categories,
            pages
        })
    }

    componentDidMount(){
        this.setState({loading: true})
        this.init();
    }

    // data indexed by localed slug
    updateDataByLocale = (locale) => {
        let categories = this.categories ? this.categories.map(c=>({
            photos: this.photos && this.photos[c.id] ? this.photos[c.id].map( ph => ({...ph,name: ph["name_"+locale]})) : [],
            name:  c["name_"+locale],
            slug: slug(c["name_"+locale]),
            description: c["description_"+locale],
        })) : [];
        let pages = this.pages ? this.pages.map(p => ({
            name: p["name_"+locale],
            slug: slug(p["name_"+locale]),
            description: p["description_"+locale]
        })) : [];
        return { pages, categories };
    }

    setLocale = (locale) => {
        if (localeOptions.indexOf( locale ) === -1) { return; }
        localStorage.setItem("locale",locale);
        const { categories, pages } = this.updateDataByLocale(locale);
        this.setState({
            locale,
            categories,
            pages
        });
        this.props.history.push(routes.HOME);
    }

    findBySlug(array,slug){
        for(let it of array) { if ( it.slug === slug) { return it; } }
        return null;
    }

    render() {
        return (
            <div className={this.state.loading ? 'loading' : '' }>
                {this.state.loading && 
                    <div className="home-page flex flex-hcenter flex-vcenter" style={{height: "100vh"}}>
                        <span>loading...</span>
                    </div>
                }
                <div className="content">
                    {!this.state.loading &&
                        <Home>
                            <Route
                                exact
                                path={routes.HOME}
                                render={() => (
                                    <HomePage
                                        locale={this.state.locale}
                                        localeOptions={localeOptions}
                                        setLocale={this.setLocale}
                                        home={this.state.home}
                                        categories={this.state.categories}
                                        pages={this.state.pages}
                                    />)}
                            />
                            <Route
                                path={routes.CATEGORY}
                                render={(props) => (
                                    <React.Fragment>
                                        <Header
                                            categories={this.state.categories} 
                                            pages={this.state.pages}
                                            locale={this.state.locale}
                                            localeOptions={localeOptions}
                                            setLocale={this.setLocale}
                                        />
                                        <CategoryPage category={this.findBySlug(this.state.categories, props.match.params.slug)} />
                                    </React.Fragment>
                                )}
                            />
                            <Route
                                path={routes.PAGE}
                                render={(props) => (
                                    <React.Fragment>
                                        <Header
                                            categories={this.state.categories} 
                                            pages={this.state.pages}
                                            locale={this.state.locale}
                                            localeOptions={localeOptions}
                                            setLocale={this.setLocale}
                                        />
                                        <PagePage page={this.findBySlug(this.state.pages, props.match.params.slug)} />
                                    </React.Fragment>
                                )}
                            />
                        </Home>
                    }
                </div>
            </div>
        )
    };
}