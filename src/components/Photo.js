import React from 'react';

export default class Photo extends React.Component{
    state = { loaded : false}
    
    render(){
       return <img src={this.props.src} alt={this.props.alt} style={{ opacity: this.state.loaded ? 1 : 0}} onLoad={e=>this.setState({loaded: true})} />
    }
}

