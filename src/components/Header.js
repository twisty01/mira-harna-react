import React from 'react';
import {Link} from 'react-router-dom';
import * as routes from '../constants/routes';

export default function Header({categories, pages, setLocale, locale, localeOptions}){
  return (
  <header className="flex flex-hspace flex-vcenter" style={{padding: "0 3rem"}} >

    <h1 className="w20" style={{padding: "0"}}><Link to={routes.HOME}>JH</Link></h1>
      
    <nav className="w60 categories" style={{padding: "0"}}>
      <ul className="flex flex-hcenter">
        {categories && Object.values(categories).map(c => <li key={c.slug}><Link className="nav-link" to={`/category/${c.slug}`}>{c.name}</Link></li>)}
      </ul>
    </nav>

    <nav className="w20" style={{padding: "0"}}>
      <ul className="flex flex-hright">
        {localeOptions.map(o => o !== locale && <li key={o}><span className="nav-link" onClick={()=>setLocale(o)}>{o}</span></li>)}
        <li><span className="divider"></span></li>
        {pages &&  Object.values(pages).map(p => <li key={p.slug}><Link className="nav-link" to={`/page/${p.slug}`}>{p.name}</Link></li>)}
      </ul>
    </nav>

  </header>
)};