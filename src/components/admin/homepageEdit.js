import React from 'react';
import * as db from '../../firebase/db';
import LoadingModal from './LoadingModal';
import { Link } from 'react-router-dom';
import { ADMIN } from '../../constants/routes';
import {getBase64} from '../../helpers/imageTools';
import { ChromePicker } from 'react-color';

const color = '#1c1c1c';
const background =  '#f5f5f5';

export default class HomepageEdit extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        home: {},
        loading: false,
        base64str: null,
      }
    }
    imageFile;

    async submit(e){
      e.preventDefault();
      this.setState({loading:true});
      try{
        await db.updateHome(this.state.home);
      } catch(e) {
        console.log(e);
      }
      this.setState({loading:false});
    }

    async handleChange(selectorFiles) {
        if ( selectorFiles.length && selectorFiles[0] ){
          const base64str = await getBase64(selectorFiles[0]);
          this.setState({base64str, home: {...this.state.home, imageFile: selectorFiles[0]}});
        }
    }

    async init(){
      let home = await db.getHome();
      this.setState({home, base64str: null});
    }

    componentDidMount(){
      this.init();  
    }

    render() {
      return (
        <React.Fragment>
          {this.state.loading && <LoadingModal/>}
          <div>
            <h2>
              <Link to={ADMIN} >{'<<'}</Link>
              Homepage edit
            </h2>
            <hr/>
            <form onSubmit={(e)=>this.submit(e)}>
              <label>Homepage Image</label>
              <input className="input" type="file" onChange={e => this.handleChange(e.target.files) }/>
              <hr/>
              <div className="flex">
                <div style={{paddingRight: '2rem'}}>
                  <label>Pages color</label>
                  <ChromePicker
                    color={ this.state.home.background || background }
                    onChangeComplete={color=>this.setState({ home: {...this.state.home, background: color.hex }})}
                  />
                  <hr/>
                  <button type="button" onClick={()=>this.setState({ home: {...this.state.home, background: null }})} >Reset</button>
                </div>
                <div>
                  <label>Heading color</label>
                  <ChromePicker
                    color={this.state.home.color || color}
                    onChangeComplete={color=>this.setState({ home: {...this.state.home, color: color.hex }})}
                  />
                  <hr/>
                  <button type="button" onClick={()=>this.setState({ home: {...this.state.home, color: null }})} >Reset</button>
                </div>
              </div>
              <hr/>
              <button disabled={this.state.loading} >Save homepage</button>
            </form>
            <img src={this.state.base64str || this.state.home.image} alt="homepage"/>
        </div>
      </React.Fragment>
    );
  }
}