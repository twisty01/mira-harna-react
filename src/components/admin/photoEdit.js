import React from 'react';
import { updatePhoto, addPhoto } from '../../firebase/db';
import ImageTools, { getBase64 }  from '../../helpers/imageTools';
import LoadingModal from './LoadingModal';

export default class PhotoEdit extends React.Component {
  constructor(p){
    super(p);
    this.state={
      name_cz: p.photo.name_cz || "",
      name_en: p.photo.name_en || "",
      imageFile: null,
      thumbnailFile: null,
      preview: null,
      loading: false,
      error: "",
    };
  }

  onChangeImage = (e) => {
    this.setState({imageFile: e.target.files[0], loading: true, error: ""});
    // create new image and thumbnail, show
    ImageTools.resize(e.target.files[0]).then(thumbnail=>{
      this.setState({thumbnailFile: thumbnail, loading: false});
    }).catch(error=>{
      this.setState({error, loading: false})
      console.log("error loading image");
      console.log(error);
    });

    getBase64(e.target.files[0]).then(res=>{
      this.setState({preview: res});
    });
  }
  
  async submit(){
    this.setState({loading: true});
    try {
      if ( this.props.photo.id !== null && this.props.photo.id !== undefined ){
        await updatePhoto(this.props.categoryId, {
            id: this.props.photo.id,
            image: this.props.photo.image,
            thumbnail: this.props.photo.thumbnail,
            name_cz: this.state.name_cz,
            name_en: this.state.name_en,
            imageFile: this.state.imageFile,
            thumbnailFile: this.state.thumbnailFile,
        });
      } else {
        await addPhoto(this.props.categoryId, {
          name_cz: this.state.name_cz,
          name_en: this.state.name_en,
          imageFile: this.state.imageFile,
          thumbnailFile: this.state.thumbnailFile
        });
      }
      this.props.onSubmit();
      this.setState({loading: false});
    } catch (error){
      this.setState({error, loading: false})
      console.log("error loading image");
      console.log(error);
    }
  }

  onSubmit = (e)=>{
    e.preventDefault();
    this.submit();
  }
  
  onDiscard = (e) => {
    e.preventDefault();
    this.props.onDiscard();
  }

  render() {
    return (
      <React.Fragment>
        {this.state.loading && <LoadingModal/>}
        <div
          className="flex flex-hcenter flex-vcenter"
          style={{
            position: 'fixed',
            left: '0',
            top: '0',
            height: '100vh',
            zIndex: '1000',
            width: '100vw',
            background: 'rgba(0,0,0,0.8)'
          }}
        >
          <div
            style={{
              background: "white",
              maxWidth: '700px',
              maxHeight: '700px',
              padding: '2em',
            }}
          >
            <h3>
              <span style={{cursor:'pointer'}} onClick={this.onDiscard} > {'<<'} </span>
              {this.props.photo.id !== null && this.props.photo.id !== undefined ? 'Edit photo' : 'Add photo'}
            </h3>
            {this.state.loading && <div>loading..</div>}
            {this.state.error && <div>{this.state.error.message}</div>}
            <form onSubmit={this.onSubmit} >
              <img src={this.props.image} accept="image/*" alt={this.props.photo.name_cz}/>
              <input placeholder="image" type="file" onChange={this.onChangeImage}/>
              <input placeholder="name_cz" type="text" value={this.state.name_cz} onChange={(e)=>this.setState({name_cz: e.target.value})}/>
              <input placeholder="name_en" type="text" value={this.state.name_en} onChange={(e)=>this.setState({name_en: e.target.value})}/>
              <button disabled={!(this.props.photo.image || (this.state.imageFile && this.state.thumbnailFile)) || this.state.loading}>{this.state.loading? 'Loading...' : 'Save photo'}</button>
            </form>
          </div>
        </div>
      </React.Fragment>
    );
  }
}