import React from 'react';
import AuthUserContext from '../auth/AuthUserContext';
import withAuthorization from '../auth/withAuthorization';
import HomepageEdit from "./homepageEdit"
import CategoriesEdit from './categoriesEdit';
import PagesEdit from './pagesEdit';
import SignOutButton from '../auth/SignOut';
import {Link, Route} from 'react-router-dom';
import * as routes from '../../constants/routes';
import CategoryEdit from './categoryEdit';
import PageEdit from './pageEdit';
import { Admin } from '../../helpers/scopedStyles';

const AccountPage = () => (
  <AuthUserContext.Consumer>
    {authUser =>
    <Admin>
      <div className="admin">

        <div className="flex flex-vcenter flex-hspace" style={{padding: "0 3rem"}}>
          <h2>{authUser.email}</h2>
          <nav style={{minHeight:0,background: "none"}}>
            <ul>
            <li><Link to={routes.HOME}> Home </Link></li>
            <li><Link to={routes.ADMIN_HOME}>Homepage edit</Link></li>
              <li><Link to={routes.ADMIN}>Categories edit</Link></li>
              <li><Link to={routes.ADMIN_PAGES}>Pages edit</Link></li>
            </ul>
          </nav>
          <SignOutButton/>
        </div>
        <hr/>

        <div className="container">
          <Route path={routes.ADMIN_HOME} component={HomepageEdit}/>
          <Route exact path={routes.ADMIN} component={CategoriesEdit}/>
          <Route path={routes.ADMIN_CATEGORY} component={CategoryEdit}/>
          <Route path={routes.ADMIN_ADD_CATEGORY} component={CategoryEdit}/>
          <Route path={routes.ADMIN_PAGES} component={PagesEdit}/>
          <Route path={routes.ADMIN_PAGE} component={PageEdit}/>
          <Route path={routes.ADMIN_ADD_PAGE} component={PageEdit}/>
        </div>
      </div>
    </Admin>
    }
  </AuthUserContext.Consumer>
);

const authCondition = (authUser) => !!authUser; //  && authUser.role === 'ADMIN';

export default withAuthorization(authCondition)(AccountPage);