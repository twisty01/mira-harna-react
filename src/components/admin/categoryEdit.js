import React from 'react';
import RichTextEditor from 'react-rte';
import * as db from '../../firebase/db';
import { ADMIN } from '../../constants/routes';
import { Link } from 'react-router-dom';
import PhotosEdit from './photosEdit';
import LoadingModal from './LoadingModal';

export default class CategoryEdit extends React.Component {
  constructor(p){
    super(p);
    this.state = {
      name_cz: "",
      name_en: "",
      description_cz: RichTextEditor.createEmptyValue(),
      description_en: RichTextEditor.createEmptyValue(),
      loading : false
    }
    this.category = {}
  }

  componentDidMount(){   
    if ( this.props.match.params && this.props.match.params.categoryId !== null && this.props.match.params.categoryId !== undefined ) {
      this.init(this.props.match.params.categoryId);
    }
  }

  init = async (categoryId) => {
    this.setState({loading: true});
    try{
      this.category = await db.getCategory(categoryId);
      this.setState({
        description_cz: RichTextEditor.createValueFromString(this.category.description_cz,'html'),
        description_en: RichTextEditor.createValueFromString(this.category.description_en,'html'),
        name_cz: this.category.name_cz,
        name_en: this.category.name_en,
        loading: false,
      })
    } catch(e){
      console.log(e);
      this.setState({loading: false});
    }
  }

  onSubmit = (e) => {
    e.preventDefault();
    if ( this.category.id !== null && this.category.id !== undefined ){
      db.updateCategory({
        id: this.category.id,
        description_cz: this.state.description_cz.toString('html'),
        description_en: this.state.description_en.toString('html'),
        name_en: this.state.name_en,
        name_cz: this.state.name_cz,
      }).then(()=>{
        this.props.history.push(ADMIN);
      }).catch(e=>{
        console.log("error updating category");
        console.log(e);
      })
    } else {
      db.addCategory({
        description_cz: this.state.description_cz.toString('html'),
        description_en: this.state.description_en.toString('html'),
        name_en: this.state.name_en,
        name_cz: this.state.name_cz,
      }).then(()=>{
        this.props.history.push(ADMIN);
      }).catch(e=>{
        console.log("error adding category");
        console.log(e);
      })
    }
  }

  render() {
    return (
      <React.Fragment>
        {this.state.loading && <LoadingModal/>}
        <h2>
          <Link to={ADMIN} > {'<<'} </Link>
          {this.category.id !== null && this.category.id !== undefined ? 'Edit category' : 'Add category'}
        </h2>
        <form onSubmit={this.onSubmit}>
          <label>Name_cz</label>
          <input className="input" name="name_cz" type="text" value={this.state.name_cz} onChange={e => this.setState({name_cz: e.target.value})}/>
          <hr/>
          <label>Name_en</label>
          <input className="input" name="name_en" type="text" value={this.state.name_en} onChange={e => this.setState({name_en: e.target.value})}/>
          <hr/>
          <label>Description_cz</label>
          <RichTextEditor value={this.state.description_cz} onChange={description_cz => this.setState({description_cz})} />
          <hr/> 
          <label>Description_en</label>
          <RichTextEditor value={this.state.description_en} onChange={description_en => this.setState({description_en})} />
          <button disabled={!(this.state.name_cz && this.state.name_en)}>Save category</button>
        </form>

        <hr/>

        {this.category.id !== null && this.category.id !== undefined && <PhotosEdit categoryId={this.category.id} />}                
      </React.Fragment>
    );
  }
}