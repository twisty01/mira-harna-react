import React from 'react';
import {
  SortableContainer,
  SortableElement,
  SortableHandle,
  arrayMove
} from 'react-sortable-hoc';
import { Link } from 'react-router-dom';
import * as db from '../../firebase/db';
import * as routes from '../../constants/routes';

const DragHandle = SortableHandle(() => <div className="drag-handle"> :: </div>); // This can be any component you want

const SortableItem = SortableElement(({value,index,onDeleteCategory}) =>
  <div className="w100 className flex flex-vcenter flex-nowrap flex-hspace">
      <div className="w20"><DragHandle/></div>
      <div className="w20">{value.name_cz}</div>
      <div className="w20">{value.name_en}</div>
      <div className="w20"><Link className="button" to={`/admin/category/${value.id}`}>edit</Link></div>
      <div className="w20"><button  onClick={() => onDeleteCategory(value, index)}>delete</button></div>
  </div>
);

const SortableList = SortableContainer(({items,onDeleteCategory}) =>
  <div>
      {items.map((value, index) => <SortableItem key={index} index={index} value={value} onDeleteCategory={onDeleteCategory}/>)}
  </div>
);

export default class CategoriesEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      selectedCategory: null,
      loading: false,
    };
  }

  onDeleteCategory = (category, index) => {
    db.deleteCategory(category).then(()=>{
      let categories = this.state.categories;
      categories.splice(index, 1);
      this.setState({categories}) 
    });
  }

  onSortEnd = ({oldIndex, newIndex}) => {
    let categories = arrayMove(this.state.categories, oldIndex, newIndex);
    for (const i in categories){
        categories[i].order = i;
    }
    this.setState({categories});
    db.updateCategoriesFromArray(categories);
  };

  async init(){
    let categories = await db.getCategoriesAsArray();
    this.setState({categories});
  }

  componentDidMount(){
    this.init();
  }

  render() {
    return (
      <React.Fragment>
        <h2>Categories edit</h2>
        <div className="w100 className flex flex-vcenter flex-nowrap flex-hspace">
            <div className="w20">catergory</div>
            <div className="w20">name_cz</div>
            <div className="w20">name_en</div>
            <div className="w20"></div>
            <div className="w20"></div>
        </div>
        <hr/>
        {this.state.categories && <SortableList 
          items={this.state.categories}
          onDeleteCategory={this.onDeleteCategory}
          onSortEnd={this.onSortEnd} 
          useDragHandle={true}
        />}
        <Link className="button" to={routes.ADMIN_ADD_CATEGORY}>Add category</Link>
      </React.Fragment>
    );
  }
}
