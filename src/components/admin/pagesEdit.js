import React from 'react';
import {
  SortableContainer,
  SortableElement,
  SortableHandle,
  arrayMove
} from 'react-sortable-hoc';
import * as db from '../../firebase/db';
import { Link } from 'react-router-dom';
import * as routes from '../../constants/routes';

const DragHandle = SortableHandle(() => <div className="drag-handle"> :: </div>); // This can be any component you want

const SortableItem = SortableElement(({ value, index, onDeletePage }) =>
  <div className="w100 className flex flex-vcenter flex-nowrap flex-hspace">
    <div className="w20"><DragHandle /></div>
    <div className="w20"><span>{value.name_cz}</span></div>
    <div className="w20"><span>{value.name_en}</span></div>
    <div className="w20"><Link className="button" to={`/admin/page/${value.id}`}>edit</Link></div>
    <div className="w20"><button  onClick={() => onDeletePage(value, index)}>delete</button></div>
  </div>
);

const SortableList = SortableContainer(({ items, onDeletePage }) =>
  <div>
    {items.map((value, index) => <SortableItem key={index} index={index} value={value} onDeletePage={onDeletePage} />)}
  </div>
);

export default class PagesEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pages: [],
      selectedPage: null,
      loading: false,
    };
  }
  
  onDeletePage = (page, index) => {
    db.deletePage(page).then(()=>{
      let pages = this.state.pages;
      pages.splice(index, 1);
      this.setState({pages}) 
    });
  }

  onSortEnd = ({ oldIndex, newIndex }) => {
    let pages = arrayMove(this.state.pages, oldIndex, newIndex);
    for (const i in pages) {
      pages[i].order = i;
    }
    this.setState({ pages });
    db.updatePagesFromArray(pages);
  };

  async init(){
    let pages = await db.getPagesAsArray();
    this.setState({ pages });
  }

  componentDidMount() {
    this.init();
  }

  render() {
    return (
      <React.Fragment>
        <h2>Pages edit</h2>
        <div className="w100 className flex flex-vcenter flex-nowrap flex-hspace">
          <div className="w20">page</div>
          <div className="w20">name_cz</div>
          <div className="w20">name_en</div>
          <div className="w20"></div>
          <div className="w20"></div>
        </div>
        <hr />
        <SortableList
          items={this.state.pages}
          onDeletePage={this.onDeletePage}
          onSortEnd={this.onSortEnd}
          useDragHandle={true}
        />
        <Link className="button" to={routes.ADMIN_ADD_PAGE}>Add page</Link>
      </React.Fragment>
    );
  }
}
