import React from 'react';

export default function({text}){
    return <div className="flex flex-hcenter flex-vcenter" style={{
        position: 'fixed',
        left: '0',
        top: '0',
        height: '100vh',
        zIndex: '1500',
        width: '100vw',
        background: 'rgba(245,245,245,0.8)'}}>
        <span>{text || 'Loading...'}</span>
    </div>
}