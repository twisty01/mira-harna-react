import React from 'react';
import RichTextEditor from 'react-rte';
import * as db from '../../firebase/db';
import { ADMIN_PAGES } from '../../constants/routes';
import { Link } from 'react-router-dom';

export default class PageEdit extends React.Component {
  constructor(p){
    super(p);
    this.state = {
      name_cz: "",
      name_en: "",
      description_cz: RichTextEditor.createEmptyValue(),
      description_en: RichTextEditor.createEmptyValue()
    }
    this.page = {}
  }

  componentDidMount(){
    this.init();
  }

  async init () {
    if (this.props.match.params && this.props.match.params.pageId !== null && this.props.match.params.pageId !== undefined ){
      this.page = await db.getPage(this.props.match.params.pageId);
      this.setState({
        description_cz: RichTextEditor.createValueFromString(this.page.description_cz,'html'),
        description_en: RichTextEditor.createValueFromString(this.page.description_en,'html'),
        name_cz: this.page.name_cz,
        name_en: this.page.name_en
      })
    }
  }

  onSubmit = (e) => {
    e.preventDefault();
    if ( this.page.id !== null && this.page.id !== undefined ){
      db.updatePage({
        ...this.page,
        description_cz: this.state.description_cz.toString('html'),
        description_en: this.state.description_en.toString('html'),
        name_en: this.state.name_en,
        name_cz: this.state.name_cz
      }).then(()=>{
        this.props.history.push(ADMIN_PAGES);
      }).catch(e=>{
        console.log("error updating page");
        console.log(e);
      })
    } else {
      db.addPage({
        ...this.page,
        description_cz: this.state.description_cz.toString('html'),
        description_en: this.state.description_en.toString('html'),
        name_en: this.state.name_en,
        name_cz: this.state.name_cz,      
      }).then(()=>{
        this.props.history.push(ADMIN_PAGES);
      }).catch(e=>{
        console.log("error updating page");
        console.log(e);
      })
    }
  }

  render() {
    return (
      <React.Fragment>
        <h2>
          <Link to={ADMIN_PAGES} > {'<<'} </Link>
          {this.page.id !== null && this.page.id !== undefined ? 'Edit page' : 'Add page'}
        </h2>
        <form onSubmit={this.onSubmit}>
          <label>Name_cz</label>
          <input className="input" name="name_cz" value={this.state.name_cz} onChange={e => this.setState({name_cz: e.target.value})}/>
          <hr/>
          <label>Name_en</label>
          <input className="input" name="name_en" value={this.state.name_en} onChange={e => this.setState({name_en: e.target.value})}/>
          <hr/>
          <label>Description_cz</label>
          <RichTextEditor value={this.state.description_cz} onChange={description_cz => this.setState({description_cz})} />
          <hr/>
          <label>Description_en</label>
          <RichTextEditor value={this.state.description_en} onChange={description_en => this.setState({description_en})} />
          <button  disabled={!(this.state.name_cz && this.state.name_en)} >Save page</button>
        </form>
      </React.Fragment>
    );
  }
}