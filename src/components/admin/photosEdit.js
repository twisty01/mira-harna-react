import React from 'react';
import PhotoEdit from './photoEdit';

import {
  SortableContainer,
  SortableElement,
  SortableHandle,
  arrayMove
} from 'react-sortable-hoc';
import * as db from '../../firebase/db';
import LoadingModal from './LoadingModal';

const DragHandle = SortableHandle(() => <div className="drag-handle"> :: </div>); // This can be any component you want

const SortableItem = SortableElement(({index, value, onDeletePhoto, onSelectPhoto}) =>
  <div className="w100 className flex flex-vcenter flex-nowrap flex-hspace">
      <div className="w20"><DragHandle/></div>
      <div className="w10"><img src={value.thumbnail || value.image} alt={value.name_cz} /></div>
      <div className="w20">{value.name_cz}</div>
      <div className="w20">{value.name_en}</div>
      <div className="w10"><button  onClick={() => onSelectPhoto(value, index)}>edit</button></div>
      <div className="w10"><button  onClick={() => onDeletePhoto(value, index)}>delete</button></div>
  </div>
);

const SortableList = SortableContainer(({items, onDeletePhoto, onSelectPhoto}) =>     
  <div>
    {items.map((value, index) => <SortableItem key={index} index={index} value={value} onDeletePhoto={onDeletePhoto} onSelectPhoto={onSelectPhoto} />)}
  </div>
);

export default class PhotosEdit extends React.Component {
  constructor(p){
    super(p);
    this.state={
      photos: [],
      selectedPhoto: null,
      loading: true,
    };
  }

  componentDidMount(){
    db.getCategoryPhotos(this.props.categoryId)
      .then(photos=>this.setState({photos, loading: false}))
      .catch(error=>{
        console.log("error getting category photos");
        console.log(error);
        this.setState({loading: false});
      })
  }

  onSelectPhoto = (photo,idx) =>{
    this.setState({selectedPhoto: photo});
  }

  onDeletePhoto = (photo,idx) => {
    this.setState({loading: true});
    db.deletePhoto(this.props.categoryId,photo).then(()=>{
      let photos = this.state.photos;
      photos.splice(idx, 1);
      this.setState({photos, loading: false});
    }).catch(e=>{
      console.log("error deleting photo");
      console.log(e);
      this.setState({loading: false});
    })
  }

  onPhotoSumbit = () => {
    this.setState({loading: true});
    db.getCategoryPhotos(this.props.categoryId)
      .then(photos => this.setState({photos, selectedPhoto: null, loading: false}))
      .catch(e=>{
        console.log("error refreshing photos after submit");
        console.log(e);
        this.setState({loading: false});
      })
  }

  onAddPhoto = () => {
    this.setState({
      selectedPhoto: {
        order: this.state.photos.length
      }
    });
  }

  onSortEnd = ({oldIndex, newIndex}) => {
    let photos = arrayMove(this.state.photos, oldIndex, newIndex);
    for (const i in photos){
      photos[i].order = i;
    }
    db.updatePhotosFromArray(this.props.categoryId, photos)
    .then(r=>this.setState({photos}))
    .catch(e=>{
      console.log("error updating photos");
      console.log(e);
    })
  };

  render() {
    return (
      <React.Fragment>
        <h3>Category Photos</h3>
        {this.state.loading && <LoadingModal></LoadingModal>}
        {this.state.photos && <SortableList
            items={this.state.photos}
            onDeletePhoto={this.onDeletePhoto}
            onSelectPhoto={this.onSelectPhoto}
            onSortEnd={this.onSortEnd} 
            useDragHandle={true}
          />}

          <button onClick={this.onAddPhoto}>Add photo</button>

          {this.state.selectedPhoto && <PhotoEdit categoryId={this.props.categoryId} photo={this.state.selectedPhoto} onSubmit={this.onPhotoSumbit} onDiscard={()=>this.setState({selectedPhoto: null})}></PhotoEdit>}
      </React.Fragment>
    );
  }
}