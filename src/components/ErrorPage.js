import React from 'react';

export default function ErrorPage({message}){
  return (
    <section>
      <h1>
        Error
      </h1>
      {message || "Page not found."}
    </section>
)};