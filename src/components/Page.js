import React from 'react';
import ErrorPage from './ErrorPage'

export default function PagePage({page, pages}){
  return (
    <div className="container">
      {page && <section>
        <h2>{page.name}</h2>
        <div dangerouslySetInnerHTML={{__html:page.description}}></div>
      </section>}
      {!page && <ErrorPage/>}
    </div>
)};