import React from 'react';
import ErrorPage from './ErrorPage';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css'; // This only needs to be imported once in your app
import Photo from './Photo';

export default class CategoryPage extends React.Component {

  constructor(p) {
    super(p);
    this.state = {
      photoIndex: 0,
      isOpen: false,
    }
  }

  render() {
    const { category } = this.props;
    const { isOpen, photoIndex } = this.state;
    return (
      <div className="container">
        {category && <section className="category-page">
          <section className="category-text container">
            <h2>{category.name}</h2>
            <div dangerouslySetInnerHTML={{__html:category.description}}></div>
          </section>

          <div className="category-photos">
            {category.photos &&
              <React.Fragment>
                <div className="grid">
                  {category.photos.map((photo, photoIndex) =>
                    <section className="photo"  key={photoIndex} onClick={() => this.setState({ isOpen: true, photoIndex })} >
                      <Photo src={photo.thumbnail || photo.image} alt={photo.name} />
                      {/* <button type="button" onClick={() => this.setState({ isOpen: true, photoIndex })}>{photo.name}</button> */}
                    </section>
                  )}
                </div>
                {isOpen &&
                  <Lightbox
                    mainSrc={category.photos[photoIndex].image}
                    nextSrc={category.photos[(photoIndex + 1) % category.photos.length].image}
                    prevSrc={category.photos[(photoIndex + category.photos.length - 1) % category.photos.length].image}
                    imageCaption={category.photos[photoIndex].name}
                    onCloseRequest={() => this.setState({ isOpen: false })}
                    onMovePrevRequest={() => this.setState({ photoIndex: (photoIndex + category.photos.length - 1) % category.photos.length }) }
                    onMoveNextRequest={() => this.setState({ photoIndex: (photoIndex + 1) % category.photos.length }) }
                  />
                }
              </React.Fragment>
            }
          </div>

        </section>}
        {!category && <ErrorPage />}
      </div>
    )
  }
};