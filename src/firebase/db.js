import { db,storage } from './firebase';

export async function getHome(){
  let res = {};
  res = await db.ref('home').once('value');
  return res.toJSON();
}

export async function updateHome(home){
  if (home.imageFile) {
    let ref = storage.ref().child("home/"+home.imageFile.name);
    let url = null;
    try {
      await ref.put(home.imageFile);
      url = await ref.getDownloadURL();
      storage.refFromURL(home.image).delete();
    } catch(e){
      console.log(e);
    }
    delete home.imageFile;
    home.image = url;
  }
  return db.ref('home').set(home);
}

// categories
export function transformPhotosIntoArray(photosObject){
  let photos = [];
  if (photosObject) {
    for (const id in photosObject) {
      photos.push({id,...photosObject[id]});
    }
    photos.sort(function(a,b){ return a.order - b.order;});
  }
  return photos;
}

export async function getCategory(categoryId){
  let res = {};
  try {
    res = await db.ref('categories/'+categoryId).once('value');
  } catch(e){
    console.log(e);
  }
  let o = res.toJSON();
  o.id = res.key;
  return o;
}

export async function getCategoryPhotos(categoryId){
  let res = {};
  try {
    res = await db.ref(`photos/${categoryId}`).once('value');
  } catch(e){
    console.log("error getting category photos");
    console.log(e);
  }
  let o = res.toJSON();
  return transformPhotosIntoArray(o);
}

export async function getPhotos(){
  let res = {};
  try {
    res = await db.ref('photos').once('value');
    res = res.toJSON();
    for (const catId in res) {
      let photosObject = transformPhotosIntoArray(res[catId]);
      res[catId] = photosObject;
    }
  } catch(e){
    console.log("error getting photos");
    console.log(e);
  }
  return res;
}

export async function getCategoriesAsArray(){
  let ref = db.ref('categories');
  let arr = [];
  try {
    let res = await ref.once('value');
    res = res.toJSON();
    for (const id in res) {
      let category = res[id];
      arr.push({...category,id});
    };
    arr.sort(function(a,b){ return a.order - b.order });
  } catch(e){
    console.log(e);
  }
  return arr;
}

export function updateCategoriesFromArray(arr){
  let categories = {};
  for( const it of arr){
    categories[it.id] = it;
  }
  return db.ref('categories').set(categories);
}

export function updateCategory(category){
  return db.ref('categories/'+category.id).set(category);
}

// return id
export function addCategory(category){
  return db.ref('categories').push(category);
}

// pages
// export async function getPagesAsObject(){
//   let res = {};
//   res = await db.ref('pages').once('value');
//   return res;
// }
export async function getPage(pageId){
  let res = {};
  try {
    res = await db.ref('pages/'+pageId).once('value');
  } catch(e){
    console.log(e);
  }
  let o = res.toJSON();
  o.id = res.key;
  return o;
}

export async function getPagesAsArray(){
  let ref = db.ref('pages').orderByChild('order');
  let res = await ref.once('value');
  let arr = [];
  res = res.toJSON();
  for (const id in res) {
    arr.push({...res[id],id});
  };
  arr.sort(function(a,b){ return a.order - b.order });
  return arr;
}

export function updatePagesFromArray(arr){
  let pages = {};
  for( const it of arr){
    pages[it.id] = it;
  }
  return db.ref('pages').set(pages);
}

export function updatePage(page){
  return db.ref('pages/'+page.id).set(page);
}

// return id
export function addPage(page){
  return db.ref('pages').push(page);
}

// photos
export function updatePhotosFromArray(categoryId, photosArr){
  let photos = {};
  for( const it of photosArr){
    photos[it.id] = {...it}
  }
  return db.ref(`photos/${categoryId}`).set(photos);
}

async function cleanPhotoStorage(photo){
  if (photo.image){
    console.log("cleaning photo image");
    try {
      await storage.refFromURL(photo.image).delete();
      photo.image = null;
    } catch(e){
      console.log("error cleaning images");
      console.log(e);
    }
  }
  if (photo.thumbnail){
    console.log("cleaning photo thumbnail");
    try {
      await storage.refFromURL(photo.thumbnail).delete();
      photo.thumbnail = null;
    } catch(e){
      console.log("error cleaning images");
      console.log(e);
    }
  }
}

async function updatePhotoStorage(categoryId,photo){
  if (photo.imageFile && photo.thumbnailFile){
    console.log("updating photo image and thumbnail");
    cleanPhotoStorage(photo);
    const stmp = Date.now();
    let refImg = storage.ref().child(`${categoryId}/${stmp}_${photo.imageFile.name}`);
    let refTn = storage.ref().child(`${categoryId}/tn_${stmp}_${photo.imageFile.name}`);
    try {
      await refImg.put(photo.imageFile);
      let newImgUrl = await refImg.getDownloadURL();
      photo.image = newImgUrl;
      delete photo.imageFile;

      await refTn.put(photo.thumbnailFile);
      let newTnUrl = await refTn.getDownloadURL();
      photo.thumbnail = newTnUrl;
      delete photo.thumbnailFile;
  } catch(e){
      console.log("error storing images");
      console.log(e);
    }    
  }
}

export async function updatePhoto(categoryId, photo){
  await updatePhotoStorage(categoryId, photo);
  console.log("updating photo");
  console.log(photo);
  return db.ref(`photos/${categoryId}/${photo.id}`).set(photo);
}

// return id
export async function addPhoto(categoryId, photo) {
  await updatePhotoStorage(categoryId, photo);
  return db.ref('photos/'+categoryId).push(photo);
}

//
//
//
// deletes

export function deletePhoto(categoryId, photo){
  cleanPhotoStorage(photo);
  return db.ref('photos/'+categoryId).child(photo.id).remove();
}

export function deleteCategory(category){
  for(const photo in category.photos){
    cleanPhotoStorage(category.photos[photo]);
  }
  return db.ref('categories').child(category.id).remove();
}

export function deletePage(page){
  return db.ref('pages').child(page.id).remove();
}
