import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';

const prodConfig = {
    apiKey: "AIzaSyDQTIic6VO90Jg6rZGt10IRBeX5ROYaw-k",
    authDomain: "jaromirharnacom.firebaseapp.com",
    databaseURL: "https://jaromirharnacom.firebaseio.com",
    projectId: "jaromirharnacom",
    storageBucket: "jaromirharnacom.appspot.com",
    messagingSenderId: "395142200080"
};

// const devConfig = {
//     apiKey: "AIzaSyBSPvisMjLjIVTaaJTDlNsa9tUYWUxPLpQ",
//     authDomain: "jaromir-harna-portfolio.firebaseapp.com",
//     databaseURL: "https://jaromir-harna-portfolio.firebaseio.com",
//     projectId: "jaromir-harna-portfolio",
//     storageBucket: "jaromir-harna-portfolio.appspot.com",
//     messagingSenderId: "8766942730"
// };

export const config = prodConfig; //process.env.NODE_ENV === 'production' ? prodConfig : devConfig;

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const db = firebase.database();
const auth = firebase.auth();
const storage = firebase.storage();

export {
    db,
    auth,
    storage
};