export const SIGN_UP = '/signup';
export const SIGN_IN = '/signin';
export const HOME = '/';

export const ADMIN = '/admin';
export const ADMIN_HOME = '/admin/home';
export const ADMIN_CATEGORIES = '/admin/categories';
export const ADMIN_CATEGORY = '/admin/category/:categoryId';
export const ADMIN_ADD_CATEGORY = '/admin/add_category';
export const ADMIN_PAGES = '/admin/pages';
export const ADMIN_PAGE = '/admin/page/:pageId';
export const ADMIN_ADD_PAGE = '/admin/add_page';

export const PASSWORD_FORGET = '/pw-forget';

export const CATEGORY = '/category/:slug';
export const PAGE = '/page/:slug';



export const localeRoutes = {
    en: {
        CATEGORY: '/category/:slug',
        PAGE: '/page/:slug'
    },
    cz: {
        CATEGORY: '/kategorie/:slug',
        PAGE: '/stranka/:slug'
    }
}