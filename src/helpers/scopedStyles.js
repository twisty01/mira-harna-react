import styled from 'styled-components'

export const Home = styled.div`
header{
  height: 3.5rem;
}
header h1{
  padding: 0 3rem;
  margin: 0;
}
h1 {
  font-size: 3.2rem;
}
h1, h2, h3 {
  margin-left:  0;
  font-weight: 300;
  margin-top: 1.5rem;
  margin-bottom: 3rem;
}
h2 {
  font-size: 2.2rem;
  margin-bottom: 1.5rem;
}
nav {
  padding: 0 2.3rem;
}
nav ul {
  list-style-type: none;  
  margin: 0 !important;
  padding: 0 !important;
}
.nav-link{
  display: inline-block;
  border: none; 
  background: none;
  text-shadow: none;
  box-shadow: none;
  outline: none;
  font-weight: 300;
  text-transform: lowercase;
  font-family: "Open sans", sans-serif;
  padding: 0 0.8rem;
  margin: 0;
  cursor: pointer;
  color:  #1c1c1c;
  text-decoration: none;
}
.home-page h1{
  padding-left: 3rem;
  padding-bottom: 0;
}
.categories .nav-link{
  font-weight: bold; 
}
.home-page .categories{
  padding-top: 0;
  padding-bottom: 2.3rem;
}
.home-page .nav-link{
  color: white;
}
.home-page .categories .nav-link{
  color:  #1c1c1c;
}
nav{
  border: none !important;
  background: none !important;
}
.content{
  transition: all 0.5s ease-in-out;
}
.loading .content{
  opacity: 0;
}
.home-page{
  width: 100vw;
  height: 100vh;
  background-size: cover;
  background-position: center;
}
.category-photos{
  margin-top: 3rem;
  margin-left: -1.5rem;
  margin-right: -1.5rem;
}
.photo {
  padding: 1.5rem;
  transition: all 0.8s;
}
.photo img{
  cursor: pointer;
  margin: auto;
  display: block;
}
.photo:hover {
  opacity: 0.9;
}
.grid {
  display: grid;
  display: -ms-grid;
  grid-template-columns: 1fr 1fr 1fr;
  -ms-grid-template-columns: 1fr 1fr 1fr;
}
.ril__caption{
  justify-content: center;
}
.photo img {
  opacity: 0;
  transition: all 0.5s ease-in-out;
}
`;

export const Admin = styled.div`
font-size: 14px;
font-family: "Open sans", sans-serif;
overflow: hidden;
color: #1c1c1c;
background: #f5f5f5;
line-height: 1.15;

.drag-handle{
  cursor: pointer;
  display: block;
  -webkit-touch-callout: none; /* iOS Safari */
  -webkit-user-select: none; /* Safari */
   -khtml-user-select: none; /* Konqueror HTML */
     -moz-user-select: none; /* Firefox */
      -ms-user-select: none; /* Internet Explorer/Edge */
          user-select: none; 
}

p, a, label, nav, q, header, div, section, ul, li, ol, td, tr, table, tbody {
  font-size: 1em;
}
input[type="text"], 
input[type="password"], 
input[type="email"], 
input, 
button{
  padding: 0.5em 1em !important;
  font-size: 1em !important;
}
button{
  margin: 0;
}
nav,
nav ul,
nav ul li {
  font-size: 1em;
}
h1{
  font-size: 2em;
  margin: 1em 0;
}
h2{
  font-size: 1.6em;
  margin: 2.5em 0 1.4em;
}
h3{
  font-size: 1.3em;
  margin: 2.5em 0 1.5em;
}
p{
  margin: 0 0 0.5em 0
} 
hr {
  margin: 1em 0 !important;
}
.public-DraftEditor-content{
  font-size: inherit;
  font-weight: 300;
  font-family: "Open sans", sans-serif;
  color: #1c1c1c;
  background: #f5f5f5;
  min-height: 4rem;
  padding: 1rem !important;
}
.DraftEditor-editorContainer{
  border-color: #ccc;
}
.button {
  cursor: pointer;
  color: #679;
  display: inline-block;
  background: #fff;
  border: 1px solid #679;
  border-radius: 2px;
  box-shadow: 0 0 0 transparent;
  text-transform: uppercase;
  text-decoration: none;
  text-align: center;
  padding: 0.5em 1em;
  font-size: 1em;
    font-weight: 700;
  line-height: 1rem;
  margin: 0;
  -webkit-appearance: none;
}
label{
  margin-bottom: 0.5rem;
}
.chrome-picker input {
  padding: 0.2rem !important;
  margin: 0 !important;
}
h1 a,
h2 a,
h3 a{
  padding-right: 1rem;
}
`;
