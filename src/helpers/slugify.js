import slugify from 'slugify';

export default function slug(str){
    return str &&  slugify(str, {
        replacement: '_',    // replace spaces with replacement
        remove: /[*+~.()'"!:@]/g,        // regex to remove characters
        lower: true          // result in lower case
      })
}